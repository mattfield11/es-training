### Reindex Command


For delegates who do not have access to their own 
localhost installation, you have 2 options to load the data into your index.
```
curl -H "Content-Type: application/json" -XPOST "http://127.0.0.1:9200/hotels/_doc/_bulk?pretty" --data-binary "@hotels.json"
```

One is to replace the IP in the command with the IP of your instance.(note that this
may change if for some reason the instance is restarted).

The second option is to copy data from some indices that have all ready been pre.loaded onto
the training machines.

For example to copy data into the imdb index:




```
POST _reindex
{
  "source": {
    "index": "dont-delete-imdb"
  },
  "dest": {
    "index": "imdb"
  }
}

```

To see the indices on your machine
```
GET _cat/indices
```