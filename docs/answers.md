
## Hotels.md

### Task - Geo query
Find all hotels in 2 km radius from the
point: 37.556035, 127.005232
(latitude, longitude)

```
GET /hotels/_search
{
    "query": {
        "bool" : {
            "must" : {
                "match_all" : {}
            },
            "filter" : {
                "geo_distance" : {
                    "distance" : "2km",
                    "location" : {
                        "lat" : 37.556035,
                        "lon" : 127.005232
                    }
                }
            }
        }
    }
}

```