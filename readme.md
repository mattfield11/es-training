## ELK Stack Training

(this is a readme for a git repository) 

The repository contains a Docker based install of Elasticsearch and Kibana.  

Follow the instructions below to install Docker and run Elasticsearch and Kibana.

The documentation and excercises for the course are in the folder DOCS.  
It is probably easiest to read the DOCS from the BitBucket web site rather than with a file editor, because of the markdown formatting.



### Hardware Requirements


* At least 4GB Ram
* 30GB or more hard disk (ideally ssd)


### Initial Installation Instructions



#### Install Instructions for Windows 10

Install Git
https://git-scm.com/download/win


Install Docker Desktop (includes Docker compose)

https://docs.docker.com/docker-for-windows/install/

You will need to share hard drive with Docker.
You will have to respond "yes" to prompt:
"Docker wants access to share drive"


#### Install Instructions For Ubuntu 1804 users



Install docker

https://docs.docker.com/install/linux/docker-ce/ubuntu/

Install docker-compose

https://docs.docker.com/compose/install/

Increase memory descriptors on system

```
sudo nano /etc/sysctl.conf
vm.max_map_count=262144
```
(reboot)
(to avoid rebooting, run:)
```
sudo sysctl -w vm.max_map_count=262144
```



#### Install and Run the Docker Compose file (For Windows and Ubuntu) 

Git clone code from this repo
Open command line terminal
From the root of this repository (where the docker-compose.yml file is)


```
docker-compose up -d
```
See logs
```
docker-compose logs --tail=500 es01
```
Restart
```
docker-compose restart <service_
```
Stop
```
docker-compose down
```

#### Test URL

Kibana URL is as follows 
http://127.0.0.1:5601

Elasticsearch URL via Curl
```
curl -X GET "127.0.0.1:9200/_cat/health?v&pretty"
```

For more information see
https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html


SSL Certificates are present in this REPO should NOT be used in production, for security.







